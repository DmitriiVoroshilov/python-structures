"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    print('Input number for factorial calculation:')
    n = int(input())
    f = 1
    for i in range(1, n + 1):
        f = f * i
    print(f)


if __name__ == "__main__":
    main()
