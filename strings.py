"""
Check whether the input string is palindrome.
"""


def main():
    """Check palindrome."""
    s = input()
    m = s[::-1]
    if m == s:
        print("yes")
    else:
        print("no")


if __name__ == "__main__":
    main()
