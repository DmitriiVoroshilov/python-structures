"""
Find common items in 2 lists without duplicates. Sort the result list before output.
"""


def main():
    """Find common numbers."""
    li1 = list(map(int, input().split()))
    li2 = list(map(int, input().split()))
    # print(sorted([i for i in li1 and li2 if i in li1 and li2]))
    x = set(li1)
    y = set(li2)
    print(list(x.intersection(y)))


if __name__ == "__main__":
    main()
