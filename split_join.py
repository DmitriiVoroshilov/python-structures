"""
split_join.py (add by yourself)
Given a string, you need to reverse the order of characters in each word within a
sentence while still preserving whitespace and initial word order.

In the string, each word is separated by single space
and there will not be any extra space in the string.
"""


def main():
    r = str(input())
    print(" ".join(r.split()[::-1])[::-1])


if __name__ == "__main__":
    main()
