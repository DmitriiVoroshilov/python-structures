"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first n contains an integer, denoting the number of commands.
Each n  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    n = int(input())
    L = []
    for i in range(n):
        n = input().split()
        command = n[0]
        if command == 'insert':
            L.insert(int(n[1]), int(n[2]))
        elif command == 'print':
            print(L)
        elif command == 'remove':
            L.remove(int(n[1]))
        elif command == 'append':
            L.append(int(n[1]))
        elif command == 'sort':
            L.sort()
        elif command == 'pop':
            L.pop()
        elif command == 'reverse':
            L.reverse()


if __name__ == "__main__":
    main()
