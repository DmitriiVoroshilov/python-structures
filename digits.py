"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    print("Input digits for summing:")
    n = input()
    sum = 0
    for digit in str(n):
        sum += int(digit)
    print(sum)


if __name__ == "__main__":
    main()
